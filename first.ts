type Nil = null

type BTreeNode<A> = Nil | {
    left: BTreeNode<A>;
    right: BTreeNode<A>;
    value: A;
}

type foldrFn<A, B> = (a: A, b: B) => B;

class BNode<T> implements BTreeNode<T> {

    constructor(public value: T,
                public left: BTreeNode<T> = null,
                public right: BTreeNode<T> = null) {

    }
}

class BTree<T> {

    constructor(public root: BTreeNode<T>) {}

    foldrTree = <B>(fn: foldrFn<T, B>, tree: BTreeNode<T>, defaultValue: B) => {
        if (!tree) return defaultValue;
        return fn(tree.value, this.foldrTree(fn, tree.left, this.foldrTree(fn, tree.right, defaultValue)))
    };

    equals(tree: BTree<T>){
        const amazingFn = (a, b) => a.toString() + b.toString();
        const res1 = this.foldrTree(amazingFn, this.root, '');
        const res2 = this.foldrTree(amazingFn, tree.root, '');
        return res1 === res2;
    }
}

const exampleBTreeInt1 = new BTree(new BNode(1, new BNode(2, new BNode(3, new BNode(4)))));
const exampleBTreeInt2 = new BTree(new BNode(1, new BNode(2, new BNode(3, new BNode(4)))));

console.log(exampleBTreeInt1.equals(exampleBTreeInt2));
//sure true

const exampleBTreeInt3 = new BTree(new BNode(10, new BNode(112, new BNode(2223, new BNode(4)))));
const exampleBTreeInt4 = new BTree(new BNode(1, new BNode(2, new BNode(3, new BNode(4)))));
console.log(exampleBTreeInt3.equals(exampleBTreeInt4));
//sure false