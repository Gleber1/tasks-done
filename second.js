class Collection {
    static of(iterable) {
        return new Collection(iterable)
    }
    [Symbol.iterator]() {
        return this._innerIterable[Symbol.iterator]()
    }
    constructor(iterable) {
        this._innerIterable = iterable
    }
    filter(func) {
        const iter = this; // because generator fn not arrow((
        return new Collection({
            [Symbol.iterator]: function* () {
                for (let item of iter) {
                    if (func(item)) {
                        yield item
                    }
                }
            }
        })
    }
    uniqByProp(prop){
        const iter = this; // because generator fn not arrow((
        return new Collection({
            [Symbol.iterator]: function* () {
                let d = [];
                for (let item of iter) {
                    const propVal = item[prop];
                    if (d.indexOf(propVal) < 0) {
                        d.push(propVal);
                        yield item;
                    }
                }
            }
        })
    }
    value() {
        return [...this];
    }
}

const events = [
    {num: 1, name: "ABC", age: 30},
    {num: 1, name: "DEF", age: 30},
    {num: 1, name: "UJN", age: 30},
    {num: 1, name: "CHC", age: 30},
    {num: 2, name: "OKN", age: 15},
    {num: 3, name: "NHF", age: 25},
    {num: 4, name: "YHB", age: 100},
    {num: 5, name: "BBB", age: 30}
];

console.log(
    Collection
        .of(events)
        .uniqByProp('num')
        .filter(e => e.age > 20)
        .value()
);