CREATE TABLE clients (
  client_id bigserial not null
    constraint clients_pk
      primary key,
  client_name text not null
);

CREATE TABLE orders (
  order_id bigserial not null
    constraint orders_pk
      primary key,
  client_id bigint
  constraint orders_fk not null
   references clients
	on delete cascade,
  order_sum bigint not null,
  order_date date default now() not null
);

create index if not exists orders
	on orders (order_sum);

    -- a)
SELECT client_name
FROM clients
  JOIN orders o ON clients.client_id = o.client_id
WHERE o.order_sum > 50;

-- b)
SELECT
  c.client_name
FROM clients c, orders o
WHERE c.client_id = o.client_id
GROUP BY c.client_id
HAVING sum(o.order_sum) > 100
