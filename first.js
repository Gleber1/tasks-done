var BNode = /** @class */ (function () {
    function BNode(value, left, right) {
        if (left === void 0) { left = null; }
        if (right === void 0) { right = null; }
        this.value = value;
        this.left = left;
        this.right = right;
    }
    return BNode;
}());
var BTree = /** @class */ (function () {
    function BTree(root) {
        var _this = this;
        this.root = root;
        this.foldrTree = function (fn, tree, defaultValue) {
            if (!tree)
                return defaultValue;
            return fn(tree.value, _this.foldrTree(fn, tree.left, _this.foldrTree(fn, tree.right, defaultValue)));
        };
    }
    BTree.prototype.equals = function (tree) {
        var amazingFn = function (a, b) { return a.toString() + b.toString(); };
        var res1 = this.foldrTree(amazingFn, this.root, '');
        var res2 = this.foldrTree(amazingFn, tree.root, '');
        return res1 === res2;
    };
    return BTree;
}());
var exampleBTreeInt1 = new BTree(new BNode(1, new BNode(2, new BNode(3, new BNode(4)))));
var exampleBTreeInt2 = new BTree(new BNode(1, new BNode(2, new BNode(3, new BNode(4)))));
console.log(exampleBTreeInt1.equals(exampleBTreeInt2));
//sure true
var exampleBTreeInt3 = new BTree(new BNode(10, new BNode(112, new BNode(2223, new BNode(4)))));
var exampleBTreeInt4 = new BTree(new BNode(1, new BNode(2, new BNode(3, new BNode(4)))));
console.log(exampleBTreeInt3.equals(exampleBTreeInt4));
//sure false 
